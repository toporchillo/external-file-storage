# Хранилище файлов для внешнего сервера #

### Требования ###
Node.js версии не ниже 12
MongoDB

### Какие функции поддерживает ###

* Простой API для скачивания файлов с удаленных серверов
* Вебхук после скачивания
* Дедубликация файлов по содержимому

### Примеры запросов ###
POST-запрос на /api/queue приказыает хранилищу: скачать и сохранить файлы по переданному URL, по завершению уведомить.
Содержимое POST-запроса - JSON такого вида:
```
{
    "apiKey":"8a6c3016b64bf89e3961437b1d4ab331101cd883",
    "sync": true,
    "files": [
        {
            "url":"https://www.bgoperator.ru/texts/memos/Memo_Turkey.pdf",
            "name":"Памятка_туриста_Турция.pdf",
            "uuid":"c76029ae-382e-4af8-890f-aaf526a155f1"
        },
        {
            "url":"https://www.bgoperator.ru/pdf?idTour=104421108710406081&a=evnew",
            "name":"Ваучер.pdf",
            "headers":{"Authorization":"Basic b24542353464567576578686786782AySA=="}
        }
    ],
    "callback":{
        "url":"https://test..ru/api/crm/index.php?action=downloaded&request_id=660003"
    }
}
```
Параметры:
- apiKey - ключ доступа к хранилищу
- sync - необязательный параметр. Если стоит true, то результат загрузки файлов придет в ответе.
Если не указан, то в callback.
- files - масив файлов, которые будут скачаны в хранилище. Если uuid не сообщить, то будет назначен автоматически.
- callback - необязательный параметр. Адрес, куда будет отправлен webhook о закачке файлов.

POST-запрос на /api/put приказыает хранилищу: сохранить содержимое из POST-запроса в виде файла, по завершению уведомить.
```
{
    "apiKey":"8a6c3016b64bf89e3961437b1d4ab331101cd883",
    "files": [
        {
            "name":"testfile.txt",
            "uuid":"c76029ae-382e-4af8-890f-aaf526a155f1",
            "body":"Test content of that file"
        }
    ],
    "callback":{
        "url":"https://nsafonov.test.crm.onlinetours.ru/api/crm/index.php?action=downloaded&request_id=660777888"
    }
}
```


### Пример ответа или содержимое коллбэка ###
Такой ответ возможен, если параметр sync стоит в true
```
[
  {
    ok: true,
    url: 'https://bgoperator.ru/pdf?idTour=104421108710406081&a=evnew',
    uuid: 'b60ee9f5-03b2-4ce8-ab05-c241f7d68aa1',
    name: 'Ваучер.pdf',
    dwl: 'http://fs.crm.onlinetours.ru:3999/dwl/b60ee9f5-03b2-4ce8-ab05-c241f7d68aa1',
    file: 'http://fs.crm.onlinetours.ru:3999/one/b5/e0/b5e024573060351f3454f4e5a2cb8435.pdf',
    new: true
  },
  {
    ok: true,
    url: 'https://bgoperator.ru/texts/memos/Memo_Turkey.pdf',
    uuid: 'c76029ae-382e-4af8-890f-aaf526a155f1',
    name: 'Памятка_туриста_Турция.pdf',
    dwl: 'http://fs.crm.onlinetours.ru:3999/dwl/c76029ae-382e-4af8-890f-aaf526a155f1',
    file: 'http://fs.crm.onlinetours.ru:3999/one/2c/46/2c46425dd235d7e445432834bcb9d4dd.pdf',
    new: false
  }
]
```

### Устрановка ###

* npm install
* правим конфиги config/env/development.js или config/env/production.js
* в файле package.json в описании npm start указан environment NODE_ENV=development
* npm start - production версия
* npm run dev - версия для разработки

