/*!
 * Коллекция файлов
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FileSchema = new Schema({
    storage: { type: String },
    path: { type: String, default: '' }, //Путь к файлу относительно корня storage
    uuid: [String], //Внешний ID
    name: { type: String, default: '' }, //Имя файла
    ext: { type: String, default: '' }, //Расширение
    md5: { type: String, default: '' }, //MD5-хэш файла
    dt: { type: Date, default: Date.now } //Дата файла (последней версии)
});

FileSchema.static('getByUUID', function(uuid, cb) {
    return this.findOne({ uuid: uuid }, function(err, file) {
        if (err) {
            return cb(err);
        }
        return cb(null, file);
    });
});

FileSchema.static('getByPath', function(path, cb) {
    return this.findOne({ path: path }, function(err, file) {
        if (err) {
            return cb(err);
        }
        return cb(null, file);
    });
});

/*
CampaignSchema.static('save', function (file, cb) {
    let self = this;
    this.findOne({path: file.path}).exec(function(err, oldfile) {
        if (err) {
            return cb(err);
        }
        if (!oldfile || !oldfile.path) {
            self.create(file, function(err, res) {
                return cb(err, res);
            })
        }
        else {
            let upd = {md5: file.md5, dt: Date.now};
            if (file.uuid) {
                upd.uuid = file.uuid;
            }
            self.update({ path: file.path }, { $set: upd }, function(err, res) {
                return cb(err, res);
            })
        }
    })
});
*/

module.exports = mongoose.model('File', FileSchema);
