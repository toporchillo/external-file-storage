require('dotenv').config();
const config = require('../../config/');
const fs = require('fs');
const Downloader = require('nodejs-file-downloader');
const md5 = require('md5');
const md5File = require('md5-file')
const async = require('async');
const path = require('path');
const request = require('request');
const File = require('../models/file.js');
const { v4: uuidv4 } = require('uuid');
const fileTypeFromFile = require('file-type').fromFile;


exports.index = function (req, res) {
    res.status(200).json({ok:true});
};

exports.dwl = function (req, res) {
    File.getByUUID(req.params.uuid, function (err, file) {
        if (err) {
            res.status(500).send("500 = Internal error");
            return;
        }
        if (!file || !file.path) {
            res.status(404).send("404 = File not found");
            return;
        }
        else {
            res.setHeader('Content-Disposition', 'inline; filename="'+encodeURIComponent(file.name)+'"');
            res.sendFile(config.storages[file.storage] + '/' + file.path);
            return;
        }
    })
}

exports.delete = function (req, res) {
    File.getByUUID(req.params.uuid, function (err, file) {
        if (!file || !file.path) {
            res.status(404).json({error:'File not found'});
        }
        else {
            File.deleteOne({uuid:req.params.uuid}, function(err, del_res) {
                fs.unlinkSync(config.storages[file.storage] + '/' + file.path);
                res.status(200).json({'ok':true});
            })
        }
    })
}

/**
 * Прямое создание файлов из данных
 * @param req HTTP-request. req.body - JSON вида
 {
    "apiKey":"8a6c3016b64bf89e3961437b1d4ab331101cd883", //Ключ
    "files": [
        {
            "name":"email_123321.json",
            "uuid":"c76029ae-382e-4af8-890f-aaf526a155f1",
            "body":"..........",
        }
    ],
    "callback":{ //После того как файлы будут сохранены, будет вызван callback
        "url":"https://test..ru/api/crm/index.php?action=downloaded&request_id=660003"
        "bundle":true //true - вызвать единый callback после скачивания Всех файлов, иначе вызов callback на каждый файл
    }
}
 * @param res HTTP-response
 */
exports.put = function (req, res) {
    let base_url = req.protocol+'://'+req.headers.host;
    if (req.body.apiKey !== config.apiKey) {
        res.status(403).json({error:'Invalid apiKey'});
        return;
    }
    let storage_vol = req.query.vol;
    if (!storage_vol || !config.storages[storage_vol]) {
        storage_vol = config.def_storage;
    }
    let ret = [];
    let completed = [];
    async.forEachLimit(req.body.files.keys(), 100, function(idx, cb) {
        let file = req.body.files[idx];
        let hash = md5(file.body);
        let uuid = file.uuid;
        if (!uuid) {
            uuid = uuidv4();
        }
        let ext = path.extname(file.name);
        let filename = hash + ext;
        let dir = filename.substr(0, 2) + '/' + filename.substr(2, 2) + '/';
        if (!fs.existsSync(config.storages[storage_vol] + '/' + dir)){
            fs.mkdirSync(config.storages[storage_vol] + '/' + dir, { recursive: true });
        }
        let filepath = dir + filename;
        fs.writeFile(config.storages[storage_vol]+'/'+filepath, file.body, function(err) {
            if (err) {
                console.log('Write data to it`s destination file error: ', err);
                completed.push({
                    error: 'Write data to it`s destination file error: ' + err.toString(),
                    error_stack: err.stack,
                    uuid: uuid,
                    name: file.name
                });
                complete(req.body, completed, res);
            } else {
                let stats = fs.statSync(config.storages[storage_vol]+'/'+filepath);
                let fileSizeInBytes = stats.size;

                saveFileToDB(storage_vol, filepath, uuid, file.name, ext, function (err, newfile) {
                    completed.push({
                        ok: true,
                        uuid: uuid,
                        ext_id: file.ext_id || 0,
                        name: file.name,
                        dwl: base_url + '/dwl/' + uuid,
                        file: base_url + '/' + storage_vol + '/' + dir + filename,
                        size: fileSizeInBytes,
                        new: newfile //Обновился файл, или нет
                    });
                    complete(req.body, completed, res);
                });
            }
        })
        ret[idx] = {ok: true, dwl: base_url + '/dwl/' + uuid};
        return cb();
    }, function (err) {
        if (!req.body.sync) {
            res.status(200).json(ret);
        }
    })
}

/**
 * Хранилище скачивает себе файлы
 * @param req HTTP-request. req.body - JSON вида
 {
    "apiKey":"8a6c3016b64bf89e3961437b1d4ab331101cd883", //Ключ
    "sync": true, //Если true, то сервис даст ответ, когда все файлы будут скачаны, иначе когда только приняты на скачивание
    "files": [
        {
            "url":"https://www.bgoperator.ru/texts/memos/Memo_Turkey.pdf",
            "name":"Памятка_туриста_Турция.pdf",
            "filetype":["pdf","docx"],
            "uuid":"c76029ae-382e-4af8-890f-aaf526a155f1"
        },
        {
            "url":"https://www.bgoperator.ru/pdf?idTour=104421108710406081&a=evnew",
            "name":"Ваучер.pdf",
            "filetype":["pdf","docx"],
            "headers":{"Authorization":"Basic b245NzdsaW5lOmd0ZiM0OFdFIWtpdWpoeSQ1Ymc="}
        }
    ],
    "callback":{ //После того как файлы будут скачаны, будет вызван callback
        "url":"https://test..ru/api/crm/index.php?action=downloaded&request_id=660003"
        "bundle":true //true - вызвать единый callback после скачивания Всех файлов, иначе вызов callback на каждый файл
    }
}
 * @param res HTTP-response
 */
exports.queue = function (req, res) {
    let base_url = req.protocol + '://' + req.headers.host;
    if (req.body.apiKey !== config.apiKey) {
        res.status(403).json({error: 'Invalid apiKey'});
        return;
    }
    let ret = [];
    let completed = [];
    async.forEachLimit(req.body.files.keys(), 100, function(idx, cb) {
        let file = req.body.files[idx];
        let tmpname = md5(file.url);
        let ext = path.extname(file.name);
        let dir = '';
        let uuid = file.uuid;
        if (!uuid) {
            uuid = uuidv4();
        }
        let params = {
            url: file.url,
            directory: config.storages[config.def_storage] + '/tmp',
            fileName: tmpname
        }
        if (file.headers) {
            params.headers = file.headers;
        }
        const dwl = new Downloader(params);
        try {
            dwl.download()
                .then(function () {
                    md5File(config.storages[config.def_storage] + '/tmp/' + tmpname).then((hash) => {
                        let filename = hash + ext;
                        dir = filename.substr(0, 2) + '/' + filename.substr(2, 2) + '/';
                        if (!fs.existsSync(config.storages[config.def_storage] + '/' + dir)) {
                            fs.mkdirSync(config.storages[config.def_storage] + '/' + dir, {recursive: true});
                        }
                        let filepath = dir + filename;

                        fs.rename(config.storages[config.def_storage] + '/tmp/' + tmpname,
                            config.storages[config.def_storage] + '/' + filepath, (err) => {
                                if (err) {
                                    console.log('Move temp file to it`s destination error: ', err);
                                    completed.push({
                                        error: 'Move temp file to it`s destination error: ' + err.toString(),
                                        error_stack: err.stack,
                                        url: file.url,
                                        uuid: uuid,
                                        name: file.name
                                    });
                                    complete(req.body, completed, res);
                                }
                                else {
                                    if (file.filetype) {
                                        fileTypeFromFile(config.storages[config.def_storage]+'/'+filepath).then((fileType) => {
                                            if (!fileType) {
                                                fileType = {ext: ''};
                                            }

                                            if (file.filetype.indexOf(fileType.ext) < 0) {
                                                completed.push({
                                                    error: 'Filetype is '+fileType.ext+' but required one of: '+file.filetype.join(', '),
                                                    error_stack: '',
                                                    url: file.url,
                                                    uuid: uuid,
                                                    name: file.name
                                                });
                                                complete(req.body, completed, res);
                                                fs.rmSync(config.storages[config.def_storage]+'/'+filepath);
                                            }
                                            else {
                                                let stats = fs.statSync(config.storages[config.def_storage]+'/'+filepath);
                                                let fileSizeInBytes = stats.size;

                                                saveFileToDB(config.def_storage, filepath, uuid, file.name, ext, function (err, newfile) {
                                                    completed.push({
                                                        ok: true,
                                                        url: file.url,
                                                        uuid: uuid,
                                                        ext_id: file.ext_id || 0,
                                                        name: file.name,
                                                        dwl: base_url + '/dwl/' + uuid,
                                                        file: base_url + '/' + config.def_storage + '/' + dir + filename,
                                                        size: fileSizeInBytes,
                                                        new: newfile //Обновился файл, или нет
                                                    });
                                                    complete(req.body, completed, res);
                                                })
                                            }
                                        })
                                    }
                                    else {
                                        let stats = fs.statSync(config.storages[config.def_storage] + '/' + filepath);
                                        let fileSizeInBytes = stats.size;

                                        saveFileToDB(config.def_storage, filepath, uuid, file.name, ext, function (err, newfile) {
                                            completed.push({
                                                ok: true,
                                                url: file.url,
                                                uuid: uuid,
                                                ext_id: file.ext_id || 0,
                                                name: file.name,
                                                dwl: base_url + '/dwl/' + uuid,
                                                file: base_url + '/' + config.def_storage + '/' + dir + filename,
                                                size: fileSizeInBytes,
                                                new: newfile //Обновился файл, или нет
                                            });
                                            complete(req.body, completed, res);
                                        })
                                    }
                                }
                        })
                    })
                })
                .catch(function (error) {
                    ret[idx] = {error: error.toString(), error_stack: error.stack};
                    completed.push({
                        error: error.toString(),
                        error_stack: error.stack,
                        url: file.url,
                        uuid: uuid,
                        name: file.name
                    });
                    complete(req.body, completed, res);
                });
        } catch (error) {
            ret[idx] = {error: error.toString(), desc: error.stack};
            completed.push({
                error: error.toString(),
                error_stack: error.stack,
                url: file.url,
                uuid: uuid,
                name: file.name
            });
            complete(req.body, completed, res);
        }
        ret[idx] = {ok: true, dwl: base_url + '/dwl/' + uuid};
        return cb();
    }, function (err) {
        if (!req.body.sync) {
            res.status(200).json(ret);
        }
    })
}

function saveFileToDB(storage_vol, filepath, uuid, filename, ext, cb) {
    let newfile = true;

    File.getByPath(filepath, function (err, oldfile) {
        if (err) {
            console.log('Get file by path MongoDB error: ', err);
        }
        // Избегаем дубликации UUID в базе
        deduppUUID(uuid, function(err, dedupp_res) {

            if (oldfile && oldfile.path) {
                File.updateOne({path: filepath}, {$addToSet: {uuid: uuid}}, function (err, res) {
                    if (err) {
                        console.log('Update file uuid in MongoDB error: ', err);
                    }
                })
                newfile = false;
            } else {
                File.create({
                    storage: storage_vol,
                    path: filepath,
                    uuid: [uuid],
                    name: filename,
                    ext: ext
                    //md5: hash
                }, function (err, res) {
                    if (err) {
                        console.log('Save file into MongoDB error: ', err);
                    }
                })
            }
            cb(err, newfile);
        })
    })
}

/**
 * Избегаем дубликации UUID в базе +++
 * @param str uuid какой UUID удалить
 * @param function cb коллбэк
 */
function deduppUUID(uuid, cb) {
    File.getByUUID(uuid, function (err, uuid_file) {
        if (err) {
            console.log('Get file by uuid MongoDB error: ', err);
            return cb(err);
        }
        if (uuid_file) {
            File.updateMany({}, {$pull: {uuid: uuid}}, function (err, res) {
                if (err) {
                    console.log('Update files MongoDB error: ', err);
                }
                return cb(err, res);
            })
        }
        else {
            return cb();
        }
    })
}

function complete(body, completed, res) {
    if (completed.length === body.files.length) {
        console.log('All complete:', completed);
        if (body.callback && body.callback.url && body.callback.bundle) {
            callback(body.callback.url, completed);
        }
        if (body.sync) {
            res.status(200).json(completed);
        }
    }

    if (body.callback && body.callback.url && !body.callback.bundle) {
        let last = completed[completed.length-1];
        callback(body.callback.url, [last]);
    }
}

function callback(url, data) {
    let options = {
        'method': 'POST',
        'url': url,
        'uri': url,
        'headers': {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)

    };
    request(options, function (error, response) {
        if (error) {
            console.log('Callback '+url+' error "'+error.toString()+'" for URL='+url, error);
        }
        else {
            console.log('Callback ' + url + ' response: ', response.statusCode, response.body);
        }
    });
}
