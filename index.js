'use strict';

require('dotenv').config();

const fs = require('fs');
const join = require('path').join;
const express = require('express');
const config = require('./config');

const mongoose = require('mongoose');

const port = process.env.PORT || config.port;
const env = process.env.NODE_ENV || 'production';

const app = express();
const connection = connect();

module.exports = {
  app, connection
};

// Bootstrap models
/*
//const models = join(__dirname, 'app/models');
fs.readdirSync(models)
  .filter(file => ~file.indexOf('.js'))
  .forEach(file => require(join(models, file)));
*/

// Bootstrap routes
require('./config/express')(app);
require('./config/routes')(app);

connection
  .on('error', console.log)
  .on('disconnected', connect)
  .once('open', listen);

function listen () {
  if (env === 'test') return;
  app.listen(port);
  console.log('App started on port ' + port + ' in ' + env + ' mode');
}

function connect () {
    var options = { /* useMongoClient: true, */ keepAlive: 1 };
    mongoose.connect(config.db, options);
    mongoose.connection.on('error', function (err) {
        console.log('MongoDB Connection error:', err.message);
    });
    mongoose.connection.once('open', function callback () {
        console.log("Connected to MongoDB");
        require('./config/init')(app);
    });
    return mongoose.connection;
}
