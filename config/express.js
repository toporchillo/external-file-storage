const express = require('express');
const session = require('express-session');
//var compression = require('compression');
//var cookieParser = require('cookie-parser');
//var cookieSession = require('cookie-session');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');

//let mongoStore = require('connect-mongo')(session);
const winston = require('winston');
require('dotenv').config();
const config = require('./');
const pkg = require('../package.json');

const env = process.env.NODE_ENV || 'production';

module.exports = function (app) {

  app.use(bodyParser.json({limit: '5mb'}));
  app.use(bodyParser.urlencoded({limit: '5mb'}));

  // Compression middleware (should be placed before express.static)
  /*
  app.use(compression({
    threshold: 512
  }));
   */

  // Static files middleware
  for (let index in config.storages) {
    app.use('/'+index, express.static(config.storages[index], { setHeaders: function (res, path, stat) {
        /*
        @todo передавать filename на основе имени файла из MongoDB
         */
        res.set('Content-Disposition', 'inline; filename="filename.pdf"');
      }}));
  }

  // Use winston on production
  var log;
  if (env !== 'development') {
    log = 'combined';
    /*
    log = {
      stream: {
        write: function (message, encoding) {
          winston.info(message);
        }
      }
    };
     */
  } else {
    log = 'combined';
  }

  // Don't log during tests
  // Logging middleware
  if (env !== 'test') app.use(morgan(log));

  // expose package.json to views
  app.use(function (req, res, next) {
    res.locals.pkg = pkg;
    res.locals.env = env;
    next();
  });

  // bodyParser should be above methodOverride
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
      // look in urlencoded POST bodies and delete it
      let method = req.body._method;
      delete req.body._method;
      return method;
    }
  }));
}
