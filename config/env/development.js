const path = require('path');

module.exports = {
port: 3999,
db: 'mongodb://localhost/storage_dev',
apiKey: '8a6c3016b64bf89e3961437b1d4ab331101cd883',
storages: { //Список папок, где будут хранится файлы хранилища, папок может быть несколько
    'one': path.normalize(__dirname + '/../../storage'),
    //2: '/mnt/external'
},
def_storage: 'one' //Папка хранилища по-умолчанию
};
