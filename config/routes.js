'use strict';
require('dotenv').config();
const config = require('./');
const api = require('../app/controllers/api');
const path = require('path');

module.exports = function (app) {

  app.get('/', function(req,res){
      res.sendFile(path.normalize(__dirname+'/../index.html'));
  });
  app.get('/dwl/:uuid', api.dwl);
  app.post('/delete/:uuid', api.delete);
  app.get('/api', api.index);
  app.post('/api/queue', api.queue);
  app.post('/api/put', api.put);

  /**
   * Error handling
   */

  app.use(function (err, req, res, next) {
    // treat as 404
    if (err.message
      && (~err.message.indexOf('Not found')
      || (~err.message.indexOf('Cast to ObjectId failed')))) {
      return next();
    }
    console.error(err.stack);
    // error page
    res.status(500).json({error: 'Internal error', desc: err.stack});
  });

  app.use(function (req, res, next) {
    res.status(404).json({error: 'Endpoint not found'});
  });
};
